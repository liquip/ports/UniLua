#define UNITY

namespace UniLua.Tools;

// thanks to dharco
// refer to https://github.com/dharco/UniLua/commit/2854ddf2500ab2f943f01a6d3c9af767c092ce75
public class ULDebug
{
    public static Action<object> Log = NoAction;
    public static Action<object> LogError = NoAction;

    static ULDebug()
    {
    }

    private static void NoAction(object msg)
    {
    }
}